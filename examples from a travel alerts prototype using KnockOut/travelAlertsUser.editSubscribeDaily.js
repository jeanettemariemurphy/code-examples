var travelAlertsUser = travelAlertsUser || {};

(function () {

    "use strict";



    travelAlertsUser.setAlertPreferences = function () {
        var localUser = {},
            dailyTravelNewsObj = {},
            alertDays = [];

        function init() {
           
            if (tfl.storage.get('travelAlertsUser')) {
                localUser = tfl.storage.get('travelAlertsUser');
                dailyTravelNewsObj = localUser.dailyTravelNewsSubscribed;
            }

           
            bindEvents();
        }

        function bindEvents() {

            ko.applyBindings(new KOAlertsModel());
        }


        /*knockout model and functions for view binding */


        function KOAlertsModel() {
            var self = this;

            self.loggedIn = localUser.loggedIn;
            self.setAlertDays = ko.observableArray(dailyTravelNewsObj.days);
            self.setOtherAlertDays = ko.observable();
            self.alertTime = ko.observable(dailyTravelNewsObj.time);
            self.subscribed = ko.observable(true);


            setAlertDays();

            function setAlertDays() {

                var matchedAlertDaysVal = false;
                    alertDays = self.setAlertDays();

                $('input[name=alert-days]').each(function (ind) {
                    var $this = $(this);
                    if(alertDays.indexOf($this.val()) === 0) {
                        $this.trigger('click');
                        matchedAlertDaysVal = true;
                    }
                });

                if (!matchedAlertDaysVal) {
                    $('input[name=alert-days]').eq(2).trigger('click');
                    self.setOtherAlertDays(true);
                    setOtherDayAlerts();

                }

                $('#alert-time').val(dailyTravelNewsObj.time).trigger('change');
                    self.alertTime(dailyTravelNewsObj.time);
            }


            function setReturnAlertDays() {

                var matchedAlertDaysVal = false;
                returnAlertDays = self.setReturnAlertDays();

                $('input[name=return-alert-days]').each(function (ind) {
                        var $this = $(this);                      
                        if(returnAlertDays.indexOf($this.val()) !== -1) {
                            $this.trigger('click');
                            matchedAlertDaysVal = true;
                         }
                    });


                    if (!matchedAlertDaysVal) {
                        $('input[name=return-alert-days]').eq(2).trigger('click');
                        self.setOtherReturnAlertDays(true);
                        setOtherReturnDayAlerts();

                    }
            }


            function setOtherDayAlerts() {
                $('input[name=week-days]').each(function () {
                    var $this = $(this);
                    
                    if (self.setAlertDays().indexOf($this.val()) !== -1) {
                        $this.trigger('click'); 
                       
                    }
                   
                });
            }

            function setOtherReturnDayAlerts() {

                $('input[name=return-week-days]').each(function () {
                    var $this = $(this);
                    
                    if (self.setReturnAlertDays().indexOf($this.val()) !== -1) {
                        $this.trigger('click'); 
                       
                    }
                });

            }



            function bindFormEvents() {

               $('input[name=alert-days]').on('change', function () {
                    alertDays = [];
                    var $this = $(this);
                    if ($this.val() === 'other') {
                        self.setOtherAlertDays(true);
                    } else  {
                        alertDays.push($this.val());
                        self.setOtherAlertDays(false);
                        self.setAlertDays(alertDays);
                        resetCheckboxes($('input[name=week-days]'), alertDays);
                    }

                });

                $('input[name=week-days]').on('change', function () {
                 
                    var $this = $(this);
                   
                     if (alertDays.indexOf($this.val()) === -1) {
                         alertDays.push($this.val());
                         self.setAlertDays(alertDays);
                     } else {
                         // remove deselected day
                         alertDays.splice(alertDays.indexOf($this.val()), 1);
                         self.setAlertDays(alertDays);
                     }

                });


                $('select#alert-time').on('change', function () {
                    if (this.selectedIndex !== 0) {
                        self.alertTime($(this).val());
                    }

                });

                 $('#signIn, #signUp').on('click', function (e) {
                    e.preventDefault();
                    self.subscribed(true);
                    self.setAlert(e.target.href);
                });

            }


            function resetCheckboxes(chks, arr) {
                chks.each(function () {
                    var $this = $(this);
                    if ($this.prop('checked')) {
                        $this.trigger('click');
                        arr.splice(alertDays.indexOf($this.val()), 1);
                    }
                });  
            }

            self.unsubscribe = function() {
                self.subscribed(false);
                self.setEditedDailyNews();
            };


            self.setEditedDailyNews = function (evt) {

                var $errors = [];

                if(self.setAlertDays().length === 0) {
                    setInvalid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'), "Please select alert days for your journey");
                    } else {
                        setValid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'));
                }
                
                $errors = $('span.field-validation-error', 'div.form-control-wrapper');

                if($errors.length === 0) {

                    var alertObj = {};
                  
                    alertObj = {
                        subscribed: self.subscribed(),
                        days: self.setAlertDays(),
                        time: self.alertTime()
                    };

                    localUser.dailyTravelNewsSubscribed = alertObj;
                    tfl.storage.set('travelAlertsUser', localUser);
                    tfl.storage.set('alertChanged', true);
                    window.location.href = "/prototype/travel-alerts/TravelAlertsManageAlertsDynamic";
                }
            };

             function setInvalid(elem, msg) {

                var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                     errMsg = msg;

                 $errorSpan.addClass('field-validation-error').html(errMsg);
            }

            function setValid(elem) {
                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                     errMsg = '';

                 $errorSpan.removeClass('field-validation-error').html(errMsg);
            }

            bindFormEvents();
        }


        return {

            init: init

        };

    }();

    travelAlertsUser.setAlertPreferences.init();

}());