var travelAlertsUser = travelAlertsUser || {};

(function(){

    "use strict";
	
	travelAlertsUser.signup = function() {

	    var $frm,
            $formTooltipIcon,
            $addressSearchBtn,
            $addressResults,
            $addressSelectBtn,
            $addressDetailsWrapper,
            $addressFields,
            $hideFields,
            $enterManually,
            $searchAgain,
            $postCode,
            $houseNo,
            $submitBtn,
            addressObj = [],
            localUser = {};
           
	    function init() {

	        if (tfl.storage.get('travelAlertsUser')) {
	           /// localUser = tfl.storage.get('travelAlertsUser');
	        }

	        cacheEleements();
	        bindEvents();
	        getAddressFinderData();
	        setCustomValidation();
	    }

	    function setCustomValidation() {
	        $.validator.addMethod("enforceTrue", function (func, el) {
	            return el.checked === true;
	        }, 'The I confirm I have read, understood and accept field is required.');
	    }

	    function cacheEleements() {
	        $frm = $('#travelAlertsSignUp');
	        $formTooltipIcon = $('span.form-tooltip-icon', $frm);
	        $addressSearchBtn = $('input#addressSearchBtn');
            $addressResults = $('#addressResults');
	        $addressSelectBtn = $('#addressSelectBtn');
	        $addressDetailsWrapper = $('div.address-results-wrapper', $frm);
	        $hideFields = $('div.hide-fields', $frm);
	        $enterManually = $('div.enter-manually', $frm);
	        $searchAgain = $('div.search-again', $frm);
	        $addressFields = $('div.address-fields', $frm);
	        $postCode = $('input#postcode');
	        $houseNo = $('input#house-no');
	        $submitBtn = $('input#submitBtn');
	    }


	    function bindEvents() {

	        $frm.on('submit', function (e) {
	            e.preventDefault();
	            $(this).validate();
	        });

	        $searchAgain.on('click', function () {
	            setAddressFinderStates('searchAgain');
	        });

	        $enterManually.on('click', function () {
	            setAddressFinderStates('enterManually');
	        });

	        $addressSelectBtn.on('click', function (e) {
	            e.preventDefault();
	            setAddressFinderStates('addressSelected');
	        });

	        $submitBtn.on('click', function (e) {
	            e.preventDefault();

	            if ($frm.valid() === true) {
	                setUserAndSubmit();
	            }
	            
            });

	        $addressResults.on('change', function (e) {
	            e.preventDefault();
	            var selected = $(this).val(),
                    tmpObj = addressObj[selected];

	            $houseNo.val(tmpObj.houseNo);
	            $('input#street').val(tmpObj.street);
	            $('input#city').val(tmpObj.city);
	            $postCode.val(tmpObj.postcode);
	            $('select#country').val(tmpObj.country).trigger('change');
	        });

	        $formTooltipIcon.each(function () {
	            var $this = $(this),
                    $tooltip = $this.next();

	            $this.on('mouseenter', function () {
	                $tooltip.addClass('show-tip');
	            }).on('mouseout', function () {
	                $tooltip.removeClass('show-tip');
	            });
	        });

	        $addressSearchBtn.on('click', function (e) {
	            e.preventDefault();        
	            var str = '', num;
	            num = Number($houseNo.val());
	              
	            if ((!isNaN(num)) && (num > 0 && num < addressObj.length + 1)) {
	                num = num -  1;
	                str += '<option value=' + num + '>' + addressObj[num].houseNo + ' ' + addressObj[num].street + ', ' + addressObj[num].city + ', ' + addressObj[num].postcode + '</option>';
	            } else {
	                for (var i = 0; i < addressObj.length; i = i + 1) {
	                    str += '<option value=' + i + '>' + addressObj[i].houseNo + ' ' + addressObj[i].street + ', ' + addressObj[i].city + ', ' + addressObj[i].postcode + '</option>';
	                }
	            }
	            $addressResults.html(str);
	            setAddressFinderStates('showAddressResults');
	        });
	    }

	    function setAddressFinderStates(state) {

	        switch(state) {
	            case 'showAddressResults':
	                $addressSearchBtn.parent().addClass('hidden');
	                $enterManually.addClass('hidden');
	                $searchAgain.removeClass('hidden');
	                $addressDetailsWrapper.removeClass('hidden');
	                $hideFields.each(function () {
	                    $(this).addClass('hidden');
	                });

	                break;

	            case 'searchAgain':
	                $addressSearchBtn.parent().removeClass('hidden');
	                $searchAgain.addClass('hidden');
	                $addressSearchBtn.removeClass('hidden');
	                $enterManually.removeClass('hidden');
	                $hideFields.each(function () {
	                    $(this).removeClass('hidden').find('input[type=text]').val('');
	                });
	                $addressFields.slideUp(300).addClass('hidden').find('input[type=text]').val('');
	                $addressDetailsWrapper.addClass('hidden').find($addressResults).empty();
	                $('select#country').val('').trigger('change');
	               
	                break;

	            case 'enterManually':
	            case 'addressSelected':
	                $addressSearchBtn.parent().addClass('hidden');
	                $enterManually.addClass('hidden');
	                $searchAgain.removeClass('hidden');
	                $addressFields.removeClass('hidden').slideDown(300);
	                $hideFields.each(function () {
	                    $(this).removeClass('hidden');
	                });
	                $addressDetailsWrapper.addClass('hidden').find($addressResults).empty();

	                break;

	            default:
	                break;
	            }
	    }

	    function setUserAndSubmit() {
            // get rid of any previous user storage
	         localStorage.removeItem('travelAlertsUser');
	        localUser = {
	            username: $('input#email').val(),
                email: $('input#email').val(),
                password: $('input#password').val(),
                firstname:  $('input#firstname').val(),
                lastname: $('input#firstname').val(),
                alerts: [],
                dailyTravelNewsSubscribed: {},
                alertsSuspended: false,
                suspendedFrom: null,
                suspendedTo: null,
	        };
	       
	        if(tfl.storage.get('tmpAlertObj')){
	            localUser.alerts.unshift(tfl.storage.get('tmpAlertObj'));
	        }

	        if (tfl.storage.get('tmpDailyemailObj')) {

	            localUser.dailyTravelNewsSubscribed = tfl.storage.get('tmpDailyemailObj');
	        }
	       
	        tfl.storage.set('travelAlertsUser', localUser);
	        localStorage.removeItem('tmpAlertObj');
	        localStorage.removeItem('tmpDailyemailObj');
	      
	       // $frm.submit();
	        window.location.href = "/prototype/travel-alerts/SignUpConfirmation";
	    }

	    function getAddressFinderData() {
	        for (var i = 0; i < 10; i = i + 1) {
	            var tmpObj = {
	                houseNo: i + 1,
	                street: 'Crown Lane Gardens',
	                city: 'London',
	                postcode: 'SW16 3HZ',
	                country: 'uk'
	            };
	            addressObj.push(tmpObj);
	        }
	    }

		return {
			init:init
		};

	}();

    travelAlertsUser.signup.init();

}());