var travelAlertsUser = travelAlertsUser || {};

(function(){

    "use strict";
	
	travelAlertsUser.signin = function() {

	    var $signInBtn,
            $validateFields,
            localUser = {},
            localUsername,
            localPassword,
            localLoggedIn;

	    function init() {
	        if (tfl.storage.get('travelAlertsUser')) {
	            localUser = tfl.storage.get('travelAlertsUser');

	        } else {
	            localUser.username = null;
	            localUser.password = null;
	            localUser.password = false;
	        }

	        $signInBtn = $('#signInBtn');
	        $validateFields = $('div.details').find('input');

	       bindEvents();
		}

		function bindEvents() {

		    $signInBtn.on('click', function (e) {

			    e.preventDefault();
			    
		        if ($('#username').val() !== localUser.username || $('#password').val() !== localUser.password) {
		            displayError();
			    }
		        else {
			        localUser.loggedIn = true;
			        tfl.storage.set('travelAlertsUser', localUser);
			        window.location.href = "/prototype/travel-alerts/TravelAlertsManageAlertsDynamic";
			    }			
		    });

		    $validateFields.on('focus', function () {
		        var $this = $(this),
                    $errorSpan = $this.closest('div.form-control-wrapper').find('span.field-validation-valid');

		        if($errorSpan.hasClass('field-validation-error')) {
		            $errorSpan.removeClass('field-validation-error').text('');
		        }
                 
		    });
		}

		function displayError() {
		    for (var i = 0; i < $validateFields.length; i = i + 1) {
		        var $el = $validateFields.eq(i),
                   $elValRef = $el.data('valid'),
                   $errorSpan = $el.closest('div.form-control-wrapper').find('span.field-validation-valid'),
                   $errMsg = $errorSpan.data('valmsg');

		        if ($el.val() !== localUser[$elValRef]) {
		            $errorSpan.addClass('field-validation-error').text($errMsg);
		        }
		    }
	    }

		return {

			init:init

		};

	}();

    travelAlertsUser.signin.init();

}());