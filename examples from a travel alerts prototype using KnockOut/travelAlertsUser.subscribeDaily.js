var travelAlertsUser = travelAlertsUser || {};

(function () {

    "use strict";

    travelAlertsUser.setAlertPreferences = function () {
        var localUser = {},
            alertDays = [];

        function init() {
           
            if (tfl.storage.get('travelAlertsUser')) {
                localUser = tfl.storage.get('travelAlertsUser');
            }

           
            bindEvents();
        }

        function bindEvents() {

            ko.applyBindings(new KOAlertsModel());
             $('select#alert-time').trigger('change');
        }


        /*knockout model and functions for view binding */


        function KOAlertsModel() {
            var self = this;

            self.loggedIn = localUser.loggedIn;
            self.setAlertDays = ko.observableArray([]);
            self.setOtherAlertDays = ko.observable(false);
            self.alertTime = ko.observable();
            self.subscribed = ko.observable();

            function bindFormEvents() {

               $('input[name=alert-days]').on('change', function () {
                    alertDays = [];
                    var $this = $(this);
                    if ($this.val() === 'other') {
                        self.setOtherAlertDays(true);
                    } else  {
                        alertDays.push($this.val());
                        self.setOtherAlertDays(false);
                        self.setAlertDays(alertDays);
                        resetCheckboxes($('input[name=week-days]'), alertDays);
                    }

                });

                $('input[name=week-days]').on('change', function () {
                 
                    var $this = $(this);
                   
                     if (alertDays.indexOf($this.val()) === -1) {
                         alertDays.push($this.val());
                         self.setAlertDays(alertDays);
                     } else {
                         // remove deselected day
                         alertDays.splice(alertDays.indexOf($this.val()), 1);
                         self.setAlertDays(alertDays);
                     }

                });


                $('select#alert-time').on('change', function () {
                    if (this.selectedIndex !== 0) {
                        self.alertTime($(this).val());
                    }

                });

                 $('#signIn, #signUp').on('click', function (e) {
                    e.preventDefault();
                    self.subscribed(true);
                    self.setAlert(e.target.href);
                });

            }


            function resetCheckboxes(chks, arr) {
                chks.each(function () {
                    var $this = $(this);
                    if ($this.prop('checked')) {
                        $this.trigger('click');
                        arr.splice(alertDays.indexOf($this.val()), 1);
                    }
                });

               
            }

            self.setAlert = function (evt) {

                var $errors = [];

                if(self.setAlertDays().length === 0) {
                    setInvalid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'), "Please select alert days for your journey");
                    } else {
                        setValid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'));
                }

                $errors = $('span.field-validation-error', 'div.form-control-wrapper');

                if($errors.length === 0) {

                    var alertObj = {};
                  
                    alertObj = {
                        subscribed: true,
                        days: self.setAlertDays(),
                        time: self.alertTime()
                    };

                    if (localUser.loggedIn) {
                        localUser.dailyTravelNewsSubscribed = alertObj;
                        tfl.storage.set('travelAlertsUser', localUser);
                        window.location.href = "/prototype/travel-alerts/TravelAlertsManageAlertsDynamic";

                    } else {

                        //store the object until the user logs in
                        tfl.storage.set('tmpDailyemailObj', alertObj);
                        window.location.href = evt;
                    }

                    tfl.storage.set('alertAdded', true);
                }
            };

             function setInvalid(elem, msg) {

                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                        errMsg = msg;

                     $errorSpan.addClass('field-validation-error').html(errMsg);
            }

            function setValid(elem) {
                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                        errMsg = '';

                     $errorSpan.removeClass('field-validation-error').html(errMsg);
                }

            bindFormEvents();
        }


        return {

            init: init

        };

    }();

    travelAlertsUser.setAlertPreferences.init();

}());