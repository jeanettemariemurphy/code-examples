
var travelAlertsUser = travelAlertsUser || {};

(function(){

	"use strict";

	travelAlertsUser.manageAlerts = function () {
	    var localUser = {},
            dailyAlertSubscribed,
            alertChanged = false,
            alertAdded = false;


	    function init() {
	        
	        if (tfl.storage.get('travelAlertsUser')) {
	            localUser = tfl.storage.get('travelAlertsUser');

	            if (!localUser.alerts) {
	                localUser.alerts = [];
	                localUser.alertsSuspended = false;
	                localUser.suspendedFrom = '';
	                localUser.suspendedTo = '';
	            }
	        }

	        if (tfl.storage.get('tmpAlertObj')) {
	            var newAlert = tfl.storage.get('tmpAlertObj');
	            localUser.alerts.unshift(newAlert);
	           	tfl.storage.set('travelAlertsUser', localUser);
	            localStorage.removeItem('tmpAlertObj');
	        }

	       if (tfl.storage.get('tmpDailyemailObj')) {
	            var dailyAlert = tfl.storage.get('tmpDailyemailObj');
	            localUser.dailyTravelNewsSubscribed = dailyAlert;
	            localStorage.removeItem('tmpDailyemailObj');
	        }

	        if (tfl.storage.get('alertChanged')) {
	            alertChanged = tfl.storage.get('alertChanged');
	            localStorage.removeItem('alertChanged');
	        }

	         if (tfl.storage.get('alertAdded')) {
	            alertAdded = tfl.storage.get('alertAdded');
	            localStorage.removeItem('alertAdded');
	        }

	        bindEvents();
	    }

	    function KOAlertsModel() {
	        var self = this;
	        self.localUser = localUser;
	        self.localAlerts = ko.observableArray(localUser.alerts);

            //TODO REVISIT THIS CODE!
	        if (localUser.dailyTravelNewsSubscribed) {
	            self.dailyAlertSubscribed = ko.observable(localUser.dailyTravelNewsSubscribed.subscribed);
	        } else {
	            self.dailyAlertSubscribed = ko.observable(false);
	        }
	      
	        self.loggedIn = ko.observable(localUser.loggedIn);
	        self.suspendFrom = ko.observable(localUser.suspendedFrom);
	        self.suspendTo = ko.observable(localUser.suspendedTo);
	        self.alertsSuspended = ko.observable(localUser.alertsSuspended);
	        self.showSuspendOptions = ko.observable(false);
	        self.alertChanged = ko.observable(alertChanged);
	        self.alertAdded = ko.observable(alertAdded);

	      
	        self.logout = function () {
	            localUser.loggedIn = false;
	            self.loggedIn(localUser.loggedIn);
	            tfl.storage.set('travelAlertsUser', localUser);
	        };

	        self.deleteAlert = function (data, e) {
	            
	            var $this = $(e.target),
                    ind = $this.data('alertid');
	                localUser.alerts.splice(ind, 1);

	            self.localAlerts(localUser.alerts);
	            tfl.storage.set('travelAlertsUser', localUser);
	            e.preventDefault();
	        };

	        self.deleteAllAlerts = function (data, e) {
	            localUser.alerts = [];
	            localUser.dailyTravelNewsSubscribed = {};
	            
	            self.localAlerts(localUser.alerts);
	            self.dailyAlertSubscribed(false);
	            tfl.storage.set('travelAlertsUser', localUser);
	           
	        };

	        self.suspendHeading = function () {
	            var msg;
	            if (self.alertsSuspended() === false) {
	                msg = "Suspend all emails";
	            } else {
	                msg = "Emails are suspended";
	            }
	            return msg;
	        };

	        self.suspendTitle = function () {
	            var msg;
	            if (self.alertsSuspended() === false) {
	                msg = "Going on holiday? Deactivate your emails for the period of time you specify";
	            } else {
	                msg = "Your emails are suspended for the following date range";
	            }
	            return msg;
	        };

	        self.deleteDailyAlert = function (data, e) {
	            e.preventDefault();
	            var alertObj = {};
	            alertObj = {
	                subscribed: false,
	                alertDays: [],
	                time: ''
	            };

	            localUser.dailyTravelNewsSubscribed = alertObj;

	            self.dailyAlertSubscribed(alertObj.subscribed);

	            tfl.storage.set('travelAlertsUser', localUser);
	        };

	        self.suspendAlerts = function () {
	          
	            self.suspendFrom($('#FromDate').val());
	            self.suspendTo($('#ToDate').val());
	            self.alertsSuspended(true);
	            self.showSuspendOptions(false);

	            localUser.alertsSuspended = true;
	            localUser.suspendedFrom =  self.suspendFrom();
	            localUser.suspendedTo = self.suspendTo();
	            tfl.storage.set('travelAlertsUser', localUser);
	        };

	        self.endSuspension = function () {
	            self.suspendFrom('');
	            self.suspendTo('');
	            self.alertsSuspended(false);

	            localUser.alertsSuspended = false;
	            localUser.suspendedFrom = self.suspendFrom();
	            localUser.suspendedTo = self.suspendTo();
	            tfl.storage.set('travelAlertsUser', localUser);
	        };

	        self.editSuspenion = function () {
	            self.showSuspendOptions(true);
	        };

	        self.supsendEmailAlerts = function(data, e){
  				e.preventDefault();
	            self.showSuspendOptions(true);
	        };
	          

	        $(document).on("click", "a#deleteAllAlerts", function (e) {
	            e.preventDefault();
	            self.deleteAllAlerts();
	            $('a.close-light-box').trigger('click');
	        });


	        $(document).on("click", "a#cancelDelete", function (e) {
	            $('a.close-light-box').trigger('click');
	        });
	       

	        $(document).on("click", "a.close-light-box", function (e) {
	            e.preventDefault();
	        });

	        $('a#cancelSuspendBtn').on('click', function (e) {
	            e.preventDefault();
	            self.showSuspendOptions(false);
	        });


	    }

	   

	    function bindEvents() {

	        ko.applyBindings(new KOAlertsModel());
	    }
		
		return {

			init:init

		};

	}();
	
	
    travelAlertsUser.manageAlerts.init();

}());