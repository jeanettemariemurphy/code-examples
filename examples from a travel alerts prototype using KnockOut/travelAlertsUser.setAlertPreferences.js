var travelAlertsUser = travelAlertsUser || {};

(function () {

    "use strict";



    travelAlertsUser.setAlertPreferences = function () {
        var localUser = {},
            localAlerts = [],
            tmpObj = {},
            alertDays = [],
            returnAlertDays = [],
            journeyStepObj = [],
            includeAccessibilityUpdates = false,
            outboundAlertTime,
            inboundAlertTime,
            returnJourneyAlert,
            startPoint,
            endPoint;


        function init() {
           
            if (tfl.storage.get('travelAlertsUser')) {
                localUser = tfl.storage.get('travelAlertsUser');
            }

            if (tfl.storage.get('tmpAlertObj')) {
                journeyStepObj = tfl.storage.get('tmpAlertObj');
            }
            bindEvents();
        }

        function bindEvents() {

            var i = journeyStepObj.length - 1;

            startPoint = journeyStepObj[0].startPoint;
            endPoint = journeyStepObj[journeyStepObj.length - 1].endPointSelected;
            
            ko.applyBindings(new KOAlertsModel());
            $('select#alert-time').trigger('change');
        }


        /*knockout model and functions for view binding */


        function KOAlertsModel() {
            var self = this;
            self.journeySteps = ko.observableArray(journeyStepObj);
            self.startPoint = ko.observable(startPoint);
            self.endPoint = ko.observable(endPoint);
            self.loggedIn = localUser.loggedIn;
            self.setReturnJourney = ko.observable(false);
            self.includeAccessibilityUpdates = ko.observable(false);
            self.setOtherAlertDays = ko.observable(false);
            self.setOtherReturnAlertDays = ko.observable(false);
            self.setAlertDays = ko.observableArray([]);
            self.setReturnAlertDays = ko.observableArray([]);

            self.toggleReturnAlert = function (data, e) {
                if (self.setReturnJourney() === false) {
                    self.setReturnJourney(true);
                    $('select#returnAlertTime').trigger('change');
                } else {
                    self.setReturnJourney(false);
                }
            };

            self.toggleAccessibility = function (data, e) {
                if (self.includeAccessibilityUpdates() === false) {
                    self.includeAccessibilityUpdates(true);
                } else {
                    self.includeAccessibilityUpdates(false);
                }
            };


            function resetCheckboxes(chks, arr) {
                chks.each(function () {
                    var $this = $(this);
                    if ($this.prop('checked')) {
                        $this.trigger('click');
                        arr.splice(alertDays.indexOf($this.val()));
                    }
                });
            }

            function bindFormEvents() {
               
                $('input[name=alert-days]').on('change', function () {
                    alertDays = [];
                    var $this = $(this);
                    if ($this.val() === 'other') {
                        self.setOtherAlertDays(true);
                    } else  {
                      
                        alertDays.push($this.val());
                        self.setOtherAlertDays(false);
                        self.setAlertDays(alertDays);
                        resetCheckboxes($('input[name=week-days]'), alertDays);
                    }
                });


                $('input[name=return-alert-days]').on('change', function () {
                    returnAlertDays = [];
                    var $this = $(this);

                    if ($this.val() === 'other') {
                        self.setOtherReturnAlertDays(true);
                    } else {
                        returnAlertDays.push($this.val());
                        self.setOtherReturnAlertDays(false);
                        self.setReturnAlertDays(returnAlertDays);
                        resetCheckboxes($('input[name=return-week-days]'), returnAlertDays);
                    }
                });

            
                $('input[name=week-days]').on('change', function () {
                 
                    var $this = $(this);
                   
                     if (alertDays.indexOf($this.val()) === -1) {
                         alertDays.push($this.val());
                         self.setAlertDays(alertDays);
                     } else {
                         // remove deselected day
                         alertDays.splice(alertDays.indexOf($this.val()), 1);
                         self.setAlertDays(alertDays);
                     }
                });

                  
                 $('input[name=return-week-days]').on('change', function () {
                    var $this = $(this);
                    if (returnAlertDays.indexOf($this.val()) === -1) {
                        returnAlertDays.push($this.val());
                        self.setReturnAlertDays(returnAlertDays);
                    } else {
                        // remove deselected day
                        returnAlertDays.splice(returnAlertDays.indexOf($this.val()), 1);
                        self.setReturnAlertDays(returnAlertDays);
                    }
                 });


                $('select#alert-time').on('change', function () {
                    var $this = $(this),
                        selectedTime = parseFloat(this.value,10);

                    if(selectedTime < 6.30 && selectedTime > 0.29 ) {
                        setInvalid($this, $("<span>You may not receive any alerts at the time you have set for this journey</span><table><thead><tr><th>First train</th><th>Last train</th></tr></thead><tbody><tr><td>06:30</td><td>00:30</td></tr></tbody></table>"));
                        } else {
                            if (this.selectedIndex !== 0) {
                                setValid($this);
                                outboundAlertTime = $(this).val();
                            }
                        }
                });


                 $('select#returnAlertTime').on('change', function () {
                    var $this = $(this),
                        selectedTime = parseFloat(this.value,10);

                    if(selectedTime < 6.30 && selectedTime > 0.29 ) {
                        setInvalid($this, $("<span>You may not receive any alerts at the time you have set for this journey</span><table><thead><tr><th>First train</th><th>Last train</th></tr></thead><tbody><tr><td>06:30</td><td>00:30</td></tr></tbody></table>"));

                        } else {
                            if (this.selectedIndex !== 0) {
                                setValid($this);
                                inboundAlertTime = $(this).val();
                            }
                        }
                });





                $('#signIn, #signUp').on('click', function (e) {
                    e.preventDefault();
                    self.setAlert(e.target.href);
                });


            }

            self.setAlert = function (evt) {

                var $errors = [];

                if(self.setAlertDays().length === 0) {
                    setInvalid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'), "Please select alert days for your journey");
                    } else {
                        setValid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'));
                    }
                if(self.setReturnJourney()=== true) {
                    if(self.setReturnAlertDays().length === 0) {
                        setInvalid($('div.radiobutton-list.return-alert-days', 'div.form-control-wrapper'), "Please select alert days for your return journey");
                        }   
                        else {
                            setValid($('div.radiobutton-list.return-alert-days', 'div.form-control-wrapper'));
                        }
                }

                if(self.setReturnJourney()=== false) {
                    setValid($('div.radiobutton-list.return-alert-days', 'div.form-control-wrapper'));
                }

                $errors = $('span.field-validation-error', 'div.form-control-wrapper');
                
                if($errors.length === 0) {

                    var alertObj = {}, stepData = [];

                    for (var i = 0; i < journeyStepObj.length; i = i + 1) {
                        var stepObj = {
                            mode: journeyStepObj[i].mode,
                            line: journeyStepObj[i].selectedLine,
                            startPoint: journeyStepObj[i].startPoint,
                            endPoint: journeyStepObj[i].endPoint
                        };
                        stepData.push(stepObj);

                    }

                    alertObj = {
                        includeAccessibilityUpdates: self.includeAccessibilityUpdates(),
                        alertDays: self.setAlertDays(),
                        returnAlertDays: self.setReturnAlertDays(),
                        outboundAlertTime: outboundAlertTime,
                        inboundAlertTime: inboundAlertTime,
                        returnJourneyAlert: self.setReturnJourney(),
                        journeyStartPoint: startPoint,
                        journeyEndPoint: endPoint,
                        steps: stepData
                    };
                  
                    

                     tfl.storage.set('alertAdded', true);

                    if (localUser.loggedIn) {
                        localUser.alerts.unshift(alertObj);
                        tfl.storage.set('travelAlertsUser', localUser);
                        localStorage.removeItem('tmpAlertObj');
                        window.location.href = "/prototype/travel-alerts/TravelAlertsManageAlertsDynamic";

                    } else {

                        //store the object until the user logs in
                        tfl.storage.set('tmpAlertObj', alertObj);
                        window.location.href = evt;
                    }
                }

            };

            function setInvalid(elem, msg) {

                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                        errMsg = msg;

                     $errorSpan.addClass('field-validation-error').html(errMsg);
            }

            function setValid(elem) {
                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                        errMsg = '';

                     $errorSpan.removeClass('field-validation-error').html(errMsg);
                }

            bindFormEvents();
        }


        return {

            init: init

        };

    }();

    travelAlertsUser.setAlertPreferences.init();

}());