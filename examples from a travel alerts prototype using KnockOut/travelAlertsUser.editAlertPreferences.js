var travelAlertsUser = travelAlertsUser || {};

(function () {

    "use strict";
    travelAlertsUser.editAlertPreferences = function () {
        var localUser = {},
            localAlerts = [],
            alertId,
            editObj = {},
            alertDays = [],
            returnAlertDays = [],
            journeyStepObj = [],
            includeAccessibilityUpdates = false,
            outboundAlertTime,
            inboundAlertTime,
            returnJourneyAlert,
            startPoint,
            endPoint;

        function init() {
            var getAlertId = window.location.href.split('?alert=');
            alertId = Number(getAlertId[1]);

            if (tfl.storage.get('travelAlertsUser')) {
                localUser = tfl.storage.get('travelAlertsUser');
                journeyStepObj = localUser.alerts;
                editObj = localUser.alerts[alertId];
            }

           
            bindEvents();
        }

        function bindEvents() {
           
            startPoint = editObj.journeyStartPoint;
            endPoint = editObj.journeyEndPoint;
            ko.applyBindings(new KOAlertsModel());
        }


        /*knockout model and functions for view binding */


        function KOAlertsModel() {
            var self = this;
            self.editAlert = ko.observable(editObj);
            self.startPoint = ko.observable(startPoint);
            self.endPoint = ko.observable(endPoint);

            self.loggedIn = localUser.loggedIn;

            self.setReturnJourney = ko.observable(editObj.returnJourneyAlert);
            self.includeAccessibilityUpdates = ko.observable(editObj.includeAccessibilityUpdates);

            self.setOtherAlertDays = ko.observable(false);
            self.setOtherReturnAlertDays = ko.observable(false);
            self.setAlertDays = ko.observableArray(editObj.alertDays);
            self.outboundAlertTime = editObj.outboundAlertTime;
            if(editObj.returnAlertDays) {
                self.setReturnAlertDays = ko.observableArray(editObj.returnAlertDays);
            } else {
                self.setReturnAlertDays = ko.observableArray([]);
            }
            

           setAlertDays();
           setUi();

            function setUi() {
                if (self.includeAccessibilityUpdates() === true) {
                    $('#setAccessibilityAlert').trigger('click');
                }

                if (editObj.returnJourneyAlert === true) {
                    $('#setReturnAlert').trigger('click');
                    $('select#returnAlertTime').trigger('change');
                    self.setReturnJourney(true);

                    $('#returnAlertTime').val(editObj.inboundAlertTime).trigger('change');
                    inboundAlertTime = editObj.inboundAlertTime;
                    setReturnAlertDays();
                }
            }


            function setAlertDays() {

                var matchedAlertDaysVal = false;
                alertDays = self.setAlertDays();

                $('input[name=alert-days]').each(function (ind) {
               
                    var $this = $(this);
                    if(alertDays.indexOf($this.val()) === 0) {
                        $this.trigger('click');
                        matchedAlertDaysVal = true;
                    }
                });

                if (!matchedAlertDaysVal) {
                    $('input[name=alert-days]').eq(2).trigger('click');
                    self.setOtherAlertDays(true);
                    setOtherDayAlerts();

                }

                $('#alert-time').val(editObj.outboundAlertTime).trigger('change');
                outboundAlertTime = editObj.outboundAlertTime;
            }


            function setReturnAlertDays() {

                var matchedAlertDaysVal = false;
                returnAlertDays = self.setReturnAlertDays();
     
                $('input[name=return-alert-days]').each(function (ind) {
                        var $this = $(this);                      
                        if(returnAlertDays.indexOf($this.val()) !== -1) {
                            $this.trigger('click');
                            matchedAlertDaysVal = true;
                         }
                    });


                    if (!matchedAlertDaysVal) {
                        $('input[name=return-alert-days]').eq(2).trigger('click');
                        self.setOtherReturnAlertDays(true);
                        setOtherReturnDayAlerts();

                    }
            }




            function setOtherDayAlerts() {
                $('input[name=week-days]').each(function () {
                    var $this = $(this);
                    
                    if (self.setAlertDays().indexOf($this.val()) !== -1) {
                        $this.trigger('click'); 
                       
                    }
                });
            }

            function setOtherReturnDayAlerts() {

                $('input[name=return-week-days]').each(function () {
                    var $this = $(this);
                    
                    if (self.setReturnAlertDays().indexOf($this.val()) !== -1) {
                        $this.trigger('click'); 
                       
                    }
                });

            }


            function resetCheckboxes(chks, arr) {
                chks.each(function () {
                    var $this = $(this);
                    if ($this.prop('checked')) {
                        $this.trigger('click');
                        arr.splice(alertDays.indexOf($this.val()));
                    }
                });
            }


            function bindFormEvents() {

               
             
                $('input[name=alert-days]').on('change', function () {

                    alertDays = [];
                    var $this = $(this);
                    if ($this.val() === 'other') {
                        self.setOtherAlertDays(true);
                    } else {

                        alertDays.push($this.val());
                        self.setOtherAlertDays(false);
                        self.setAlertDays(alertDays);

                        resetCheckboxes($('input[name=week-days]'), alertDays);
                    }
                });


                $('input[name=return-alert-days]').on('change', function () {
                    returnAlertDays = [];
                    var $this = $(this);

                    if ($this.val() === 'other') {
                        self.setOtherReturnAlertDays(true);
                    } else {

                        returnAlertDays.push($this.val());
                        self.setOtherReturnAlertDays(false);
                        self.setReturnAlertDays(returnAlertDays);
                        resetCheckboxes($('input[name=return-week-days]'), returnAlertDays);
                    }
                });


                $('input[name=week-days]').on('change', function () {

                    var $this = $(this);

                    if (alertDays.indexOf($this.val()) === -1) {
                        alertDays.push($this.val());
                        self.setAlertDays(alertDays);
                    } else {
                        // remove deselected day
                        alertDays.splice(alertDays.indexOf($this.val()), 1);
                        self.setAlertDays(alertDays);
                    }
                });


                $('input[name=return-week-days]').on('change', function () {
                    var $this = $(this);
                    if (returnAlertDays.indexOf($this.val()) === -1) {
                        returnAlertDays.push($this.val());
                        self.setReturnAlertDays(returnAlertDays);
                    } else {
                        // remove deselected day
                        returnAlertDays.splice(returnAlertDays.indexOf($this.val()), 1);
                        self.setReturnAlertDays(returnAlertDays);
                    }
                });

               $('select#alert-time').on('change', function () {
                    var $this = $(this),
                        selectedTime = parseFloat(this.value,10);

                    if(selectedTime < 6.30 && selectedTime > 0.29 ) {
                        setInvalid($this, $("<span>You may not receive any alerts at the time you have set for this journey</span><table><thead><tr><th>First train</th><th>Last train</th></tr></thead><tbody><tr><td>06:30</td><td>00:30</td></tr></tbody></table>"));
                        } else {
                            if (this.selectedIndex !== 0) {
                                setValid($this);
                                outboundAlertTime = $(this).val();
                            }
                        }
                });


                 $('select#returnAlertTime').on('change', function () {
                    var $this = $(this),
                        selectedTime = parseFloat(this.value,10);

                    if(selectedTime < 6.30 && selectedTime > 0.29 ) {
                        setInvalid($this, $("<span>You may not receive any alerts at the time you have set for this journey</span><table><thead><tr><th>First train</th><th>Last train</th></tr></thead><tbody><tr><td>06:30</td><td>00:30</td></tr></tbody></table>"));

                        } else {
                            if (this.selectedIndex !== 0) {
                                setValid($this);
                                inboundAlertTime = $(this).val();
                            }
                        }
                });

               
                self.toggleAccessibility = function (data, e) {
                    if (self.includeAccessibilityUpdates() === false) {
                        self.includeAccessibilityUpdates(true);
                    } else {
                        self.includeAccessibilityUpdates(false);
                    }
                };


                self.toggleReturnAlert = function (data, e) {
                    $('select#returnAlertTime').trigger('change');
                    if (self.setReturnJourney() === false) {
                        self.setReturnJourney(true);
                    } else {
                        self.setReturnJourney(false);
                    }
                };

           
            }


            self.deleteAlert = function (data, e) {

                var $this = $(e.target);
                localUser.alerts.splice(alertId, 1);
                tfl.storage.set('travelAlertsUser', localUser);
                window.location.href = "/prototype/travel-alerts/TravelAlertsManageAlertsDynamic";
                e.preventDefault();
            };

            self.setEditedAlert = function (evt) {

                var $errors = [];

                if(self.setAlertDays().length === 0) {
                    setInvalid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'), "Please select alert days for your journey");
                    } else {
                        setValid($('div.radiobutton-list.alert-days', 'div.form-control-wrapper'));
                    }
                if(self.setReturnJourney()=== true) {
                    if(self.setReturnAlertDays().length === 0) {
                        setInvalid($('div.radiobutton-list.return-alert-days', 'div.form-control-wrapper'), "Please select alert days for your return journey");
                        }   
                        else {
                            setValid($('div.radiobutton-list.return-alert-days', 'div.form-control-wrapper'));
                        }
                }

                if(self.setReturnJourney()=== false) {
                    setValid($('div.radiobutton-list.return-alert-days', 'div.form-control-wrapper'));
                }

                $errors = $('span.field-validation-error', 'div.form-control-wrapper');


                if($errors.length === 0) {
                    var alertObj = {
                        includeAccessibilityUpdates: self.includeAccessibilityUpdates(),
                        alertDays: self.setAlertDays(),
                        returnAlertDays: self.setReturnAlertDays(),
                        outboundAlertTime: outboundAlertTime,
                        inboundAlertTime: inboundAlertTime,
                        returnJourneyAlert: self.setReturnJourney(),
                        journeyStartPoint: editObj.journeyStartPoint,
                        journeyEndPoint: editObj.journeyEndPoint,
                        steps: editObj.steps

                    };
                  
                        localUser.alerts[alertId] = alertObj;         
                        tfl.storage.set('travelAlertsUser', localUser);
                        tfl.storage.set('alertChanged', true);
                        window.location.href = "/prototype/travel-alerts/TravelAlertsManageAlertsDynamic"; 
                    }

            };

            function setInvalid(elem, msg) {
                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                        errMsg = msg;

                     $errorSpan.addClass('field-validation-error').html(errMsg);
             }

            function setValid(elem) {
                 var $this = elem,
                    $errorSpan = $this.parents('div.form-control-wrapper').find('.field-validation-valid'),
                        errMsg = '';

                     $errorSpan.removeClass('field-validation-error').html(errMsg);
                }

         bindFormEvents();
        }


        return {

            init: init

        };

    }();

    travelAlertsUser.editAlertPreferences.init();

}());