var travelAlertsUser = {};

(function () {

    "use strict";

    travelAlertsUser = function () {
	    
	    function init() {

	        // load station data
	        if (tfl.storage.get('travelAlertsStationData') === undefined || !tfl.storage.get('travelAlertsStationData')) {
	            setStationsData();
	        } else {
	            console.log('station data is in storage');
	        }

	       
	    }

	    function setUserData() {
	      var url = "/cdn/static/scripts/prototype/travel-alerts/travelAlertsTestUser.json";

	        $.ajax({
	            dataType: "json",
	            url: url
	        }).done(function (data) {

	           tfl.storage.set('travelAlertsUser', data);
	        }).fail(function () {
	            console.log('data load failed');
            });  
	    }

	    function setStationsData() {
	        var url = "/cdn/static/scripts/prototype/travel-alerts/typeahead-alerts-data.json";
	        $.ajax({
	            dataType: "json",
	            url: url
	        }).done(function (data) {
	            tfl.storage.set('travelAlertsStationData', data);
	        }).fail(function () {
	            console.log('station data load failed');
	        });
	    }


		return {

			init:init

		};

	}();

	travelAlertsUser.init();

}());