
var travelAlertsUser = travelAlertsUser || {};

(function(){

	"use strict";

	

	travelAlertsUser.createAlert = function () {
	    var localUser = {},
            localStationData = [],  // store the stations json from storage it it's loaded
            datumObj = {},
            nextStep = {},
            journeyStepObj = [],
            filteredLinesData = [],
            filteredStationData = {},
            editing = false,
            multiLineRoutes = ['circle', 'hammersmith-city'];
	  

		function init() {

		    if (tfl.storage.get('travelAlertsUser')) {
		        localUser = tfl.storage.get('travelAlertsUser');
		    }

		   if (tfl.storage.get('tmpAlertObj')) {
		        journeyStepObj = tfl.storage.get('tmpAlertObj');
		        editing = true;
		        localStorage.removeItem('tmpAlertObj');
		    } 
		    bindEvents();
         
		}

		function bindEvents() {
		    ko.applyBindings(new KOAlertsModel());
		}


	    /*knockout model and functions for view binding */


		function KOAlertsModel() {
		    var self = this;
		   
		    /* flags */
		    self.startStep = ko.observable(true);
		   
		    self.selectLines = ko.observable(false);
		    self.journeyComplete = ko.observable(false);
		    self.createEndPoint = ko.observable(false);
		    self.stepLine = ko.observable();
		    self.chooseNextLine = ko.observable(true);
		    self.loggedIn = localUser.loggedIn;

            if(journeyStepObj.length){
				self.journeySteps = ko.observableArray(journeyStepObj);
				
				self.startStep(true);
            } else {
            	self.journeySteps = ko.observableArray([]);
            }
		    
		    self.startPoint = ko.observable();
		    self.endPointSelected = ko.observable();
		    self.lines = ko.observableArray();
		    self.selectedLine = ko.observableArray();
		    
		    self.selectedLineDisplayName = ko.observable();

		    // remove these?
		    self.hasMultiLine = ko.observable(false);
		    self.multiLineObj = ko.observable();



		    function setStationData () {

		        var stationData = new Bloodhound({
		            name: 'stationData',
		             prefetch: {
		           		url: '/cdn/static/scripts/prototype/travel-alerts/typeahead-alerts-data.json',
		           		ttl: 0,
		           		filter: function (stations) {
		               		return $.map(stations, function (station) {

		                   	return {
		                       stationName: station.name,
		                       modes: station.modes,
		                       lines: station.lines
		                   	};
		               	});
		           	},
		      	 },
		        
		            datumTokenizer: function (d) {
		                return Bloodhound.tokenizers.whitespace(d.stationName);
		            },
		            queryTokenizer: Bloodhound.tokenizers.whitespace,
		        });

		        var promise = stationData.initialize();
		        promise
                 .done(function () { console.log('success!'); })
                 .fail(function () { console.log('err!'); });

		        var compiledTemplate = Hogan.compile("<div class='mode-icons'>{{#modes}}<span class='mode-icon {{.}}-icon'>&nbsp;</span>{{/modes}}</div><span class='stop-name'>{{stationName}}</span>");

		        $('#bloodhound .typeahead').typeahead(null, {
		            name: 'stationData',
		            displayKey: 'stationName',
		            source: stationData.ttAdapter(),
		            templates: {
		                suggestion: compiledTemplate.render.bind(compiledTemplate)
		            }
		        }).on('typeahead:selected', function (obj, datum) {

		            datumObj.startPoint = datum.stationName;
		            datumObj.lines = datum.lines;
		        });

		       if(editing) {
		    		datumObj = journeyStepObj;
		    		self.endPointSelected(datumObj[journeyStepObj.length-1].endPointSelected);
		    		self.setCompletedAlertForEditing();
	    		}
		    }


		    self.selectStartPoint = function (data, e) {
		        var tags = [],
                   matched = [];
		       
		        e.preventDefault();
		        self.startPoint(datumObj.startPoint);

		        for (var i = 0; i < datumObj.lines.length; i = i + 1) {
		            tags.push(datumObj.lines[i].id);
		            datumObj.lines[i].tags = tags;
		            tags = [];
		            var ind = multiLineRoutes.indexOf(datumObj.lines[i].id);
		               
		            if (ind !== -1) {                  
		                matched.push(multiLineRoutes[ind]);
		               
		            }
		        }

		        if (matched.length > 1) {
		            setMultiLineOptions(matched);
		        } else {

		            if (datumObj.lines.length === 1) {
		                self.lines(datumObj.lines);
		                self.startStep(false);
		                self.selectLines(false);
		                self.selectLine(datumObj.lines[0]);
		            }
		            else {
		                self.lines(datumObj.lines);
		       
		                self.startStep(false);
		                self.selectLines(true);
		            }
		        }
		    };


		    function setMultiLineOptions(arr) {
		        var matchedStr = arr.join();

		        switch (matchedStr) {

		            case 'circle,hammersmith-city':

		               var multiLines = {
		                    id: 'circle-hammersmith-city',
		                    tags: ['circle', 'hammersmith-city'],
		                    name: "Circle / Hammersmith & City"
		                };

		                datumObj.lines.unshift(multiLines);
		                self.multiLineObj(datumObj.multiLines);

		                filterLines();
		                break;
		        } 
		    }

		    function filterLines() {
		        var filtered = datumObj.lines.filter(function (line){
		            if ((line.id !== "circle" && line.id !=="hammersmith-city")) {
		                return line;
		            }
		
		        });
		       
		        datumObj.lines = filtered;
		        self.startStep(false);
		        self.lines(datumObj.lines);
		        self.selectLines(true);
		    }
	      

		    self.selectEndPoint = function (data, e) {
		        e.preventDefault();
		        self.endPointSelected(datumObj.endPointSelected);


		        self.createEndPoint(false);
		    };

		    self.selectLine = function (data, e) {

		        if (data.id === 'dlr') {
		            datumObj.mode = 'dlr';
		        } else if (data.id === 'overground') {
		            datumObj.mode = 'overground';
		        } else {
		            datumObj.mode = 'tube';
		        }

		       
		       datumObj.selectedLine = data.tags;
		       self.selectLines(false);
		       self.chooseNextLine(false);
		       self.createEndPoint(true);
		       self.selectedLine(datumObj.selectedLine);
		       self.selectedLineDisplayName(data.name);
		       self.stepLine(data.id);
		       initFilteredTypeahead(data.id);
		    };


		    self.continueJourney = function (data, e) {
		        e.preventDefault();
		        datumObj.selectedLineDisplayName = self.selectedLineDisplayName();
		        journeyStepObj.push(datumObj);
		        datumObj = nextStep;
		        nextStep = {};
		        self.resetUi();
		    };

		    self.resetUi = function () {
		        self.journeySteps(journeyStepObj);
		        self.endPointSelected(null);
		        self.startPoint(datumObj.startPoint);
		        self.chooseNextLine(true);

		        var tags = [],
                   matched = [];
		        for (var i = 0; i < datumObj.lines.length; i = i + 1) {
		            tags.push(datumObj.lines[i].id);
		            datumObj.lines[i].tags = tags;
		            tags = [];
		            var ind = multiLineRoutes.indexOf(datumObj.lines[i].id);

		            if (ind !== -1) {
		                matched.push(multiLineRoutes[ind]);
		            }
		        }

		        if (matched.length > 1) {
		            setMultiLineOptions(matched);
		        } else {
		           
		            if (datumObj.lines.length === 1) {
		                self.lines(datumObj.lines);
		                self.startStep(false);
		                self.selectLines(false);
		                self.selectLine(datumObj.lines[0]);
		            } else {

		            	self.lines(datumObj.lines); 
		            	self.startStep(false);
		            	self.selectLines(true);
		            } 
		        }

		     };


		    self.completeJourney = function () {
		        self.journeyComplete(true);
		        datumObj.nextStep = nextStep;
		        journeyStepObj.push(datumObj);
		        // push the journey object to temp storage in case not signed in	       
		        tfl.storage.set('tmpAlertObj', journeyStepObj);
		        window.location.href = "/prototype/travel-alerts/TravelAlertsCreateAlertPreferences"; 
		    };



		self.pushEditState = function() {
			if(self.selectLines() === true) {
				console.log('prev self.selectLines heading back to startStep');
				self.startStep(true);
				self.startPoint(null);
				self.selectLines(false);
				self.endPointSelected(null);
				self.createEndPoint(false);
			}

			if(self.createEndPoint() === true) {
				console.log('prev self.createEndPoint heading back to selectLines');
				self.lines(datumObj.lines); 
				self.endPointSelected(null);
				self.createEndPoint(false);
			    self.chooseNextLine(true);
			    self.selectLines(true);

			    // don't display available lines if there's only 1
			    if(datumObj.lines.length > 1){
					self.selectLines(true);
				} else if(journeyStepObj.length === 0) {
					self.startStep(true);
				} else {
					self.selectLines(false);
					self.setStepForEditing();
				}
			}

			if(self.endPointSelected()!==null) {
				console.log('prev self.endPointSelected heading back to createEndPoint');
				var dataObj = {}, obj=[];
				
				obj = datumObj.lines.filter(function(line){
					if (line.id === self.stepLine()) {
			                return line;
			            }
					});

				dataObj = {
					id: obj[0].id,
					name: obj[0].name,
					tags: obj[0].tags
				};
				self.selectLine(dataObj);
				self.createEndPoint(true);
			    self.endPointSelected(null);
			}
			
		};


	//TODO combine setStepForEditing and setCompletedAlertForEditing
		self.setStepForEditing = function() {
			var currentStepLine = "",
				stepToSplice = journeyStepObj.length-1;

			datumObj = journeyStepObj[stepToSplice];
			
			journeyStepObj.splice(stepToSplice,1);

			if(datumObj.selectedLine.length === 1) {
				currentStepLine = datumObj.selectedLine[0];
			} else {
				currentStepLine = datumObj.selectedLine.join('-');
			}

			self.stepLine(currentStepLine);
			self.journeySteps(journeyStepObj);
			self.startPoint(datumObj.startPoint);
			
			var dataObj = {}, obj=[];

			obj = datumObj.lines.filter(function(line){
				if (line.id === self.stepLine()) {
		                return line;
		            }
				});

			dataObj = {
				id: obj[0].id,
				name: obj[0].name,
				tags: obj[0].tags
			};

			self.selectLine(dataObj);
			self.createEndPoint(true);
		    self.endPointSelected(null);
		};



		self.setCompletedAlertForEditing = function() {
			
			var currentStepLine = "",
				stepToSplice = journeyStepObj.length-1;

			datumObj = journeyStepObj[stepToSplice];
			journeyStepObj.splice(stepToSplice,1);

			if(datumObj.selectedLine.length === 1) {
				currentStepLine = datumObj.selectedLine[0];
			} else {
				currentStepLine = datumObj.selectedLine.join('-');
			}
			self.stepLine(currentStepLine);
			self.journeySteps(journeyStepObj);
			self.startPoint(datumObj.startPoint);
			self.selectedLine(datumObj.selectedLine);
			var dataObj = {}, obj=[];
			obj = datumObj.lines.filter(function(line){
				if (line.id === self.stepLine()) {
		                return line;
		            }
				});

			dataObj = {
				id: obj[0].id,
				name: obj[0].name,
				tags: obj[0].tags
			};

			nextStep = datumObj.nextStep;
			self.startStep(false);
			self.chooseNextLine(false);
			self.createEndPoint(false);
			self.lines(datumObj.lines);
			
		};

		function initFilteredTypeahead(line) {
		   
		   var filteredStationData = new Bloodhound({
		       name: 'filteredStationData',

		       prefetch: {
		           url: '/cdn/static/scripts/prototype/travel-alerts/line-data/' + line + '.json',
		           ttl: 0,
		           filter: function (stations) {
		               return $.map(stations, function (station) {

		                   return {
		                       stationName: station.name,
		                       modes: station.modes,
		                       lines: station.lines
		                   };
		               });
		           },
		       },
		        datumTokenizer: function (d) {
		            return Bloodhound.tokenizers.whitespace(d.stationName);
		        },
		        queryTokenizer: Bloodhound.tokenizers.whitespace,
		    });


		   var promise = filteredStationData.initialize(),
               promise2 = filteredStationData.initialize(true);

		    promise
             .done(function () { console.log('filtered typeahead success!'); })
             .fail(function () { console.log('filtered typeahead serr!'); });

		    promise2
            .done(function () { console.log('filtered typeahead success!'); })
            .fail(function () { console.log('filtered typeahead serr!'); });

		    var compiledTemplate = Hogan.compile("<div class='mode-icons'>{{#modes}}<span class='mode-icon {{.}}-icon'>&nbsp;</span>{{/modes}}</div><span class='stop-name'>{{stationName}}</span>");


		    $('#filtered .typeahead').typeahead(null, {
		        name: 'filteredStationData',
		        displayKey: 'stationName',
		        source: filteredStationData.ttAdapter(),
		        templates: {
		            suggestion: compiledTemplate.render.bind(compiledTemplate)
		        }

		    }).on('typeahead:selected', function (obj, datum) {
		        nextStep.startPoint = datum.stationName;
		        nextStep.lines = datum.lines;
               
		        datumObj.endPointSelected = datum.stationName;
		        console.log('nextStep.startPoint set', nextStep.startPoint);
		    });
		}

		setStationData();


		}

		return {

		    init: init

		};

	}();

    travelAlertsUser.createAlert.init();

}());