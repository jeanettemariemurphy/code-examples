tyres.factory('filterService', ['$http', '$q',
    function($http, $q) {

        var filterObj = {  // holds the arrays of the possible dropdown options
                sectionWidth:   [],
                profileRatio:   [],
                rimDiameter:    [],
                loadRating:     [],
                speedRating:    []
            },
            selectedFilterObj = {   // predefine skeleton object so clearFilters will loop and clear mandatory properties not sent to clearFilters
                action:         '',
                sectionWidth:   '',
                profileRatio:   '',
                rimDiameter:    '',
                loadRating:     '',
                speedRating:    ''
            }; // holds the user selected values
 
        var getNextFilter = function(action) {
            clearFilters(action);
            // set the action for the ? parameters
            selectedFilterObj.action = action;
            var deferred = $q.defer();
            $http({
                method: 'GET',
                params: selectedFilterObj,
                url: tyres.urls.getTyresByFilters
            })
                .success(function(result, status) {
                    if (result.responseInfo.desc === 'SUCCESS') {

                        // we only wnat the values for our dropdown, ditch the rest
                        var tempAr = [];
                        angular.forEach(result.data, function(item) {
                            tempAr.push(item.value);
                        });
                        angular.copy(tempAr, filterObj[action]);
                        deferred.resolve();
                    } else {
                        deferred.reject('error setFilterParams, not found');
                    }
                }).
            error(function(data, status) {
       
                deferred.reject('error getTyresBySpec');
            });

            return deferred.promise;
        };

         var clearFilters = function(action) {
            var clearNext = false;
            for (var property in selectedFilterObj) {
                if (selectedFilterObj.hasOwnProperty(property) && action == property) {
    
                    clearNext = true;
                }
                if (clearNext) {
                    selectedFilterObj[property] = "";
                    filterObj[property] = [];
                }
             } 
        };

        var resetFilters = function() {
            filterObj.sectionWidth = [];
            filterObj.profileRatio = [];
            filterObj.rimDiameter = [];
            filterObj.loadRating = [];
            filterObj.speedRating = [];
        };

        return {
            filterObj: filterObj,
            getNextFilter: getNextFilter,
            selectedFilterObj: selectedFilterObj,
            resetFilters: resetFilters
        };

    }
]);