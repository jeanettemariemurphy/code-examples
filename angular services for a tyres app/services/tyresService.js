tyres.factory('tyresService', ['$http', '$q',
    function($http, $q) {

        var car = {}, 
            tyreGroups = [], 
            tyresObjFront = {}, 
            tyresObjRear = {}, 
            sortOrder = [
                {
                    text: 'Audi Genuine tyres',
                    val: 'audiGenuine'
                }, {
                    text: 'Price (low to high)',
                    val: 'priceIncVat'
                }
            ];

        /* car info
        =========================================================================== */

        var getTyresByCarReg = function(carReg) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
               url: tyres.urls.getTyresByCarReg + carReg + "?groupBy=wheelSize"
            })
                .success(function(result, status) {
         
                    if (result.responseInfo.desc === "SUCCESS") {
                       
                        result.data.carInfo.vrm = $.trim(result.data.carInfo.vrm);
                        angular.copy(result.data.carInfo, car);
                        angular.copy(result.data.groups,  tyreGroups);
                        deferred.resolve(result);
                    } else {
                        
                        deferred.reject('error getTyresByCarReg, not found');
                    }
                }).
                error(function(data, status) {
                    deferred.reject('error getTyresByCarReg');
                });

            return deferred.promise;
        };

        
        var getTyresBySpec = function(spec, rearTyre) {
            var deferred = $q.defer();
                $http({
                    method: 'GET',
                    params:spec,
                    url: tyres.urls.getTyresBySpec
                })
                .success(function(result, status) {
                    if (result.responseInfo.desc === 'SUCCESS') {
                      if(rearTyre) {
                         angular.copy(result.data, tyresObjRear);
                        } else {
                        angular.copy(result.data, tyresObjFront);
                        }
                    } 
                     deferred.resolve();
                }).
                error(function(data, status) {
                    deferred.reject('error_loading_date_info');
                });

            return deferred.promise;
        };


        return {
            car: car,
            tyreGroups:       tyreGroups,
            tyresObjFront:    tyresObjFront,
            tyresObjRear:     tyresObjRear,
            sortOrder:        sortOrder,      
            getTyresByCarReg: getTyresByCarReg,
            getTyresBySpec:   getTyresBySpec
        };

    }
]);