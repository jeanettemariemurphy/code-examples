tyres.factory('basketService', ['$cookies', '$cookieStore',
    function($cookies, $cookieStore) {

        var basket = {
            items: [],
            totals:{
                amount:0,
                qty:0
            }
        };

        var addToBasket = function(obj) {

            if (typeof obj.qtyInBasket === "undefined") {
                obj.qtyInBasket = 0;
            }

            // add to basket
            var inBasket = false;
            angular.forEach(basket.items,function(item,inde){
                    // see if there is an item with the same partNumber already in the basket
                    // if there is then use that
                if(item.partNumber == obj.partNumber) 
                    {
                        obj.qtyInBasket = item.qtyInBasket + obj.amount;
                        basket.items[inde] = obj;
                        inBasket = true;
                    }
            });

           if (!inBasket) { // has an item already
   
                obj.qtyInBasket = obj.qtyInBasket + obj.amount;
                basket.items.push(obj); 
           }

            obj.amount = 1; //reset dropdown back to 1
            $cookieStore.put('basket',basket.items);
             setBasketTotals();
        };

        var removeFromBasket = function(obj) {
            obj.qtyInBasket--;
            if(obj.qtyInBasket === 0) {
                deleteFromBasket(obj);
            }
            $cookieStore.put('basket',basket.items);
            setBasketTotals();
        };

        var syncBasketWithResults = function(obj){
            angular.forEach(basket.items,function(item,inde){
                if(item.partNumber == obj.partNumber) 
                    {
                        obj.qtyInBasket = item.qtyInBasket;
                        basket.items[inde] = obj;
                    }
            });
        };

        var deleteFromBasket = function(obj) {
            
            // clear down the item in the basket 
            obj.qtyInBasket = 0;
        
           var index = basket.items.indexOf(obj);
            basket.items.splice(index, 1);
            $cookieStore.put('basket',basket.items);
            if(basket.items.length === 0) {
                removeBasketCookie();
            }  
             setBasketTotals();
        };


        var setBasketTotals = function() {
             var totals ={
                amount:0,
                qty:0
            };
            angular.forEach(basket.items, function(item) {
                totals.amount += item.priceIncVat * item.qtyInBasket;
                totals.qty += item.qtyInBasket;

            });

            angular.copy(totals,basket.totals);
            
        };

        var clearBasket = function() {
            basket.items = [];
            basket.totals.amount = 0;
            basket.totals.qty = 0;
        };

        var removeBasketCookie = function() {
             $cookieStore.remove('basket');
        };

        var loadBasketResults = function() {
            basket.items = $cookieStore.get('basket');
            setBasketTotals();
        };


        return {
            basket: basket,
            addToBasket: addToBasket,
            removeFromBasket: removeFromBasket,
            deleteFromBasket: deleteFromBasket,
            removeBasketCookie: removeBasketCookie,
            clearBasket: clearBasket,
            loadBasketResults: loadBasketResults,
            syncBasketWithResults:syncBasketWithResults
        };

    }
]);