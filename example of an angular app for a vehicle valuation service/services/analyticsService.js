partExchange.factory('analyticsService', ['partExchangeService', 'dealerService', '$analytics',
    function(partExchangeService, dealerService, $analytics) {

        var tags = {};

        var setOmnitureTag = function(evt, fieldname) {
            switch (evt) {
                case 'step1Pageload':
                    tags = $analytics.eventTrack('load', null, {
                        prop1: '/explore-models/part-exchange-start.html',
                        pageName: 'Part Exchange Form start',
                        prop2: 'en',
                        prop3: 'UK',
                        event99: 'event99',
                    });
                    break;
                case 'step2Pageload':

                    tags = $analytics.eventTrack('load', null, {
                        prop1: '/explore-models/part-exchange-details.html',
                        pageName: 'Part Exchange Form Confirm Details'

                    });
                    break;

                case 'step3Pageload':

                    tags = $analytics.eventTrack('load', null, {
                        prop1: '/explore-models/part-exchange-confirmation.html',
                        pageName: 'Contact Us Form Comfirmation',
                        event100: 'event100'

                    });
                    break;

                case 'getVehicleDetailsSubmit':
                    tags = $analytics.eventTrack('click', null, {
                        prop11: 'pe:' + partExchangeService.vehicleDetails.vrm,
                        prop12: 'pe:' + partExchangeService.vehicleDetails.mileage + '|' + partExchangeService.userDetails.enquiryPurpose,
                        eVar41: 'prop12'
                    });
                    break;

                case 'fieldCompleteSuccess':

                    tags = $analytics.eventTrack('click', null, {
                        prop1: '/explore-models/part-exchange.html/' + fieldname + '.html'
                    });
                    break;

                case 'fieldCompleteError':

                    tags = $analytics.eventTrack('click', null, {
                        prop4: 'pe_' + fieldname
                    });
                    break;


                case 'startOver':

                    tags = $analytics.eventTrack('click', null, {
                        event3: 'event3'

                    });
                    break;

                case 'downloadPDF':
                    tags = $analytics.eventTrack('click', null, {
                        prop39: 'pdf-download|good-' + partExchangeService.valuationDetails.valuations.exact.good + '|average-' + partExchangeService.valuationDetails.valuations.exact.average + '|poor-' + partExchangeService.valuationDetails.valuations.exact.poor
                    });
                    break;

                case 'centreSelected':
                    var formattedDealerName = dealerService.dealer.name.split(' ').join('-').toLowerCase();
                    tags = $analytics.eventTrack('click', null, {
                        prop1: '/part-exchange/centre-information/' + formattedDealerName + '.html'
                    });
                    break;

                case 'centreSearchSuccess':
                    tags = $analytics.eventTrack('click', null, {
                        prop59: fieldname,
                        event51: 'event51',
                        event52: 'event52'
                    });
                    break;


                case 'centreSearchNotFound':
                    tags = $analytics.eventTrack('click', null, {
                        prop59: fieldname,
                        event51: 'event51'
                    });
                    break;

            }

            return tags;

        };

        return {
            setOmnitureTag: setOmnitureTag
        };

    }
]);
