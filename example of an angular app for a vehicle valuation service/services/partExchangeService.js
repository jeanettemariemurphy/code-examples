partExchange.factory('partExchangeService',
    function(apiService, $q,$cookieStore, $http) {

        var vehicleDetails = {};
        var userDetails = {};
        var valuationDetails = {};
        var dealers = {};
       

        var cache = {
            createOwnerRecordCreated: false
        };


        var createGuid = function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();

        };

        var createCookie = function() {

            var ownersGuid = $cookieStore.get("ownersGuid");
            if(typeof ownersGuid ==="undefined"){
                var newOwnersGuid = createGuid();
                $cookieStore.put("ownersGuid",newOwnersGuid);
            }

        };

        var getVehicleDetailsByVrm = function() {
           
            createCookie();
            var cleanVrm = vehicleDetails.vrm.replace(/ /g, '');
            var deferred = $q.defer();

            apiService.withErrorHandling({
                title: 'getVehicleDetailsByVrm',
                url: partExchange.urls.getVehicleDetailsByVrm + cleanVrm,
                method: 'GET'
            }).then(function(result) {
                // add the mileage to the vehicle results
                
                result.data.mileage = vehicleDetails.mileage;
                angular.copy(result.data, vehicleDetails);

                deferred.resolve();
            }, function(err) {
                deferred.reject('error getVehicleDetailsByVrm');
            });

            return deferred.promise;
        };


        var createOwnerRecord = function() {
            createCookie();
            var deferred = $q.defer();

            var userDetailsToJson = angular.toJson(userDetails);

            if (cache.createOwnerRecordCreated) {
                deferred.resolve();
            } else {

               $http({
                     url: partExchange.urls.createOwnerRecord,
                     headers: {
                       'Content-Type': 'application/json'
                     },
                    data:  userDetailsToJson,
                    method: 'POST'
                }).then(function(result) {

                    cache.createOwnerRecordCreated = true;
                    deferred.resolve();

                }, function(error) {

                    deferred.reject();
                });
            }


            return deferred.promise;

        };

        var getValuation = function(params) {
            var cleanVrm = vehicleDetails.vrm.replace(/ /g, '');
            var deferred = $q.defer();

              createCookie();
           

            apiService.withErrorHandling({
                title: 'getValuation',
                url: partExchange.urls.getValuation + cleanVrm,
                params: params,
                method: 'GET'
            }).then(function(result) {

                angular.copy(result.data, valuationDetails);
                deferred.resolve();

            }, function(error) {
                deferred.reject();
            });


            return deferred.promise;

        };

        var startOver = function() {
            var clearedVehiclObj = {};
            angular.copy(clearedVehiclObj, vehicleDetails);
            userDetails.enquiryPurpose = '';
            angular.copy(userDetails.enquiryPurpose);
        };


        var clearAllData = function() {
            var emptyObj = {};
            angular.copy(emptyObj, vehicleDetails);
            angular.copy(emptyObj, userDetails);
            angular.copy(emptyObj, valuationDetails);
            angular.copy(emptyObj, dealers);
        };

        return {
            vehicleDetails: vehicleDetails,
            userDetails: userDetails,
            valuationDetails: valuationDetails,
            getVehicleDetailsByVrm: getVehicleDetailsByVrm,
            createOwnerRecord: createOwnerRecord,
            getValuation: getValuation,
            createCookie:createCookie,
            startOver: startOver,
            clearAllData: clearAllData
        };

    });