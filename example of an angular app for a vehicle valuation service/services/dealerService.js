partExchange.factory('dealerService', ['$http', '$q', '$filter', 'partExchangeService',
    function($http, $q, $filter, partExchangeService ) {

        var dealers = {};
        var dealer = {};
        //var 

        var getDealers = function(limit, usersLocation) {
            var params = "&limit=" + limit + "&place=" + usersLocation;
            partExchangeService.createCookie();
            var deferred = $q.defer();
            if (!usersLocation) {
                deferred.reject('No userLocation');
            }
            $http({
                method: 'GET',
                url: partExchange.urls.findDealerships + params
            })
                .success(function(result, status) {

                    angular.copy(result, dealers);

                    deferred.resolve();
                }).
            error(function(data, status) { 
                deferred.reject('error getDealers');
            });

            return deferred.promise;

        };

         var getDealerDetails = function(dealerCode) {

            partExchangeService.createCookie();
            var deferred = $q.defer();

            if (partExchange.cacheChecker.getDealerDetails === dealerCode) {
                deferred.resolve();
            } else {
                $http({
                    method: 'GET',
                    url: partExchange.urls.getDealerDetails + dealerCode
                })
                    .success(function(result, status) {
                        result.services = [];
                        angular.copy(result, dealer);
                      
                        partExchange.cacheChecker.getDealerDetails = dealerCode;

                        deferred.resolve();
                    }).
                error(function(data, status) {
                    deferred.reject();
                });

            }
            return deferred.promise;


        };

        return {
            dealers: dealers,
            dealer: dealer,
            getDealers: getDealers,
            getDealerDetails: getDealerDetails
        };

    }
]);
