partExchange.directive('vehicleDetailsForm',
    function(configService) {
        return {
            restrict: 'EA',
            templateUrl: '/etc/audi-ui/apps/part-exchange/ng-components/directives/vehicleDetailsForm.html',
            controller: function($scope, partExchangeService, stepsService, analyticsService, $timeout) {

                var fieldErrorArr = [],
                    fieldSuccessArr = [];


                $scope.getVehicleDetails = function() {

                    $scope.vehicleDetailsForm.submitted = true;
                    $scope.partExchange.loading = true;
                    $scope.partExchange.serverErrors = {};

                    if ($scope.vehicleDetailsForm.$valid) {

                        partExchangeService.getVehicleDetailsByVrm().then(function(data) {
                            $timeout(function() {
                                $scope.partExchange.loading = false;
                                 $scope.setCurrentStep(2);
                            }, 500);
                           
                            analyticsService.setOmnitureTag('getVehicleDetailsSubmit');

                        }, function(data) {
                            
                            $scope.partExchange.loading = false;
                            $scope.partExchange.serverErrors.registration = "We are sorry, but we cannot find vehicle on the database.  Please double check the registration number your provided.";
                        });

                    } else {

                        $timeout(function() {
                            analyticsService.setOmnitureTag('fieldCompleteError', fieldErrorArr);
                            fieldErrorArr = [];
                        }, 500);

                        $scope.partExchange.loading = false;
                    }


                };



                // Utility functions
                // -----------------

                $scope.fieldHasError = function(formName, fieldName) {
                    var form = $scope[formName],
                        field = form[fieldName],
                        returnVal = false;

                    if (field.$invalid && (form.submitted || field.$hadFocus)) {
                        returnVal = true;
                        if ($.inArray(fieldName, fieldErrorArr) === -1) {
                            fieldErrorArr.push(fieldName);
                        }
                       
                        if ($.inArray(fieldName, fieldSuccessArr) !== -1) {
                            fieldSuccessArr.splice($.inArray(fieldName, fieldSuccessArr), 1);
                        }
                    }

                    return returnVal;
                };

                $scope.fieldHasSuccess = function(formName, fieldName) {

                    var form = $scope[formName],
                        field = form[fieldName],
                        returnVal = false;

                    if (field.$valid && (form.submitted || field.$hadFocus)) {

                        returnVal = true;

                        if ($.inArray(fieldName, fieldSuccessArr) === -1) {
                            analyticsService.setOmnitureTag('fieldCompleteSuccess', fieldName);
                            fieldSuccessArr.push(fieldName);
                        }

                        if ($.inArray(fieldName, fieldErrorArr) !== -1) {
                            fieldErrorArr.splice($.inArray(fieldName, fieldErrorArr), 1);
                        }
                    }
                    return returnVal;
                };

                $scope.fieldHasFeedback = function(formName, fieldName) {
                    var form = $scope[formName],
                        field = form[fieldName];
                    return form.submitted || field.$hadFocus ? true : false;
                };

            }
        };
    }
);
