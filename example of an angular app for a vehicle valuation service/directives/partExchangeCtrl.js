partExchange.directive('partExchange',
    function(configService) {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: '/etc/audi-ui/apps/part-exchange/ng-components/directives/partExchange.html',
            link: function($scope, $el) {
                $el.find('a.trigger').on('click', function(e){
                    e.preventDefault();
                });
            },
            controller: function($scope, partExchangeService, stepsService, dealerService, analyticsService,templateCache,$location) {

                partExchangeService.createCookie();
                // dev mode
               
                $scope.partExchange = {
                    vehicleDetails: partExchangeService.vehicleDetails,
                    userDetails: partExchangeService.userDetails,
                    valuationDetails: partExchangeService.valuationDetails,
                    dealers: dealerService.dealers,
                    dealer: dealerService.dealer,
                    currentStep:  1,
                    serverErrors: {},
                    loading: false
                };

                $scope.audiEmailRegex = getAudiEmailRegex();
                
                $scope.setCurrentStep = function(step) {
                    $scope.steps = stepsService.steps;
                    $scope.partExchange.currentStep = step;
                    return $scope.partExchange.currentStep;
                };

                $scope.formatEnquiryPurpose = function(str) {
                    return str.replace(/-/g, ' ');
                };

                $scope.toggleTerms = function() {
                    $scope.showTerms = !$scope.showTerms;
                };

                $scope.startOver = function() {
                    partExchangeService.startOver();
                    $scope.partExchange.currentStep = 1;
                    analyticsService.setOmnitureTag('startOver');
                };

                $scope.getServerError = function(fieldName) {
                   
                    if ($scope.partExchange.serverErrors[fieldName] !== undefined) {
                        return $scope.partExchange.serverErrors[fieldName];
                    } else {
                        return false;
                    }
                };

                $scope.clearAllData = function(){
                    partExchangeService.clearAllData();      
                };

                var init = function() {
                    $scope.steps = stepsService.steps;
                    $scope.showTerms = false;
                    analyticsService.setOmnitureTag('step1Pageload');
                };
            

                init();
            }
        };
    }
);
