
partExchange.directive('valuation', 
	function(configService) {
		return {
			restrict: 'EA',
			templateUrl: '/etc/audi-ui/apps/part-exchange/ng-components/directives/valuation.html',
			controller: function ($scope,analyticsService) {

				var today = new Date();
				var plus10 = new Date($scope.partExchange.valuationDetails.producedAt);
				plus10.setDate(plus10.getDate()+10);

				$scope.expiryDate = plus10;
				analyticsService.setOmnitureTag('step3Pageload');

				$scope.showCondtionDetails = false;

				$scope.showVCD = function() {
					$scope.showCondtionDetails = !$scope.showCondtionDetails;
				}

				$scope.downloadPDF = function(){
					analyticsService.setOmnitureTag('downloadPDF');
				};
			}
		};
	}
);

